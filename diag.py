
import obd_com
import serial_obd
import time
import pid
import sqlite3
import data_save

ser = obd_com.obd_init('/dev/ttyACM0')


#conn = sqlite3.connect('ma_base.db')

#cursor = conn.cursor()
#cursor.execute("""
#CREATE TABLE IF NOT EXISTS users(
     #id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
     #name TEXT,
     #age INTERGER
#)
#""")

#cursor.execute("""
#INSERT INTO users(name, age) VALUES(?, ?)""", ("olivier", 30))

#cursor.execute("""SELECT name, age FROM users""")
#user1 = cursor.fetchone()
#print(user1)

#conn.commit()

#conn.close

def pid_format(in_string, pid_req):
    if in_string != "NO DATA": # if PID unsuported, returns NO DATA
        #if PID returns 1 byte of data
        if pid_req.bytes_number == 1:
            result_A = int(in_string,16) #byte converted from string to hexa
            result = pid_req.compute(result_A)
            
        #if PID returns 2 bytes of data
        elif pid_req.bytes_number == 2:
            result_A =int(in_string[0:2], 16) #first byte
            result_B = int(in_string[2:4], 16) #second byte
            result = pid_req.compute(result_A, result_B)
            
        #if PID returns 4 bytes of data
        elif pid_req.bytes_number == 4:
            result_A =int(in_string[0:2], 16) #first byte
            result_B = int(in_string[2:4], 16) #second byte
            result_C = int(in_string[4:6], 16) #third byte
            result_D = int(in_string[6:8], 16) #fourth byte
            result = pid_req.compute(result_A, result_B, result_C, result_D)
            
    return result
    

while True:
    command = input("Enter command: ")
    
    if command == '1':
        
        num = input("Entrer le PID: ")

        #create PID object
        pid_req = pid.Pid(int(num))
        
        #write request
        obd_com.pid_write(ser, pid_req)
        
        #read answer
        str_read = obd_com.pid_read(ser, pid_req.bytes_number)
        
        #format
        result = pid_format(str_read)

        #display
        print(pid_req.description, end = '') #print PID description
        print(": ", end = '')
        if result != "NO DATA":
            print(result, end = '') #print returned value
            print(" ", end = '')
            print(pid_req.unit) #print units
        else:
            print(result) #print "NO DATA"
    
    elif command == '2':
        obd_com.faults_request(ser)
        result = obd_com.faults_read(ser)
        print(result[0:1])
        print(result[2:3])
        print(str('0'))
        if result[0] == 48:
            print("P", end='')
        elif result[0] == '1':
            print("C", end='')
        elif result[0:1] == '2':
            print("B", end='')
        elif result[0:1] == '3':
            print("U", end='')
        
        if result[1] == '0':
            print("0", end='')
        elif result[1] == 49:
            print("1", end='')
        elif result[1] == '2':
            print("2", end='')
        elif result[1] == '3':
            print("3", end='')
            
        print(result[3:5].decode("Ascii"), end='')
        print(result[6:8].decode("Ascii"), end='')
        
    elif command == '3':
        col_num = int(input("How many PID do you want? "))
        
        pid_num = [None] * col_num
        pid_req = [None] * col_num
        for i in range(col_num):
            pid_num[i] = input("PID: ")
            
            #create PID object
            pid_req[i] = pid.Pid(int(pid_num[i]))
            
            
        compteur = 0
        while True:
            data_save.file_write("fichier", compteur)
            data_save.file_write("fichier", " ")
            str_read = []
            result = [None] * col_num
            for i in range(col_num):
                #write request
                obd_com.pid_write(ser, pid_req[i])
                
                #read answer
                str_read.append(obd_com.pid_read(ser, pid_req[i].bytes_number))
                
                #format
                result[i] = pid_format(str_read[i], pid_req[i])
                
                #write line in file
                data_save.file_write("fichier", result[i])
                data_save.file_write("fichier", " ")
            
            compteur = compteur +1
            data_save.file_write("fichier", "\r")
            
            time.sleep(1)
            
        
