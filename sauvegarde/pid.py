

class Pid:
    number = 0
    bytes_number = 0
    description = ''
    unit = ''
    
    def __init__(self, num):
        self.number =  num
        if num == 0:
            self.bytes_number = 4
            self.description = 'PID supported [01 - 20]'
            self.unit = ''
        elif num == 1:
            self.bytes_number = 4
            self.description = 'Monitor status since DTC cleared'
            self.unit = ''
        elif num == 2:
            self.bytes_number = 2
            self.description = 'Freeze DTC'
            self.unit = ''
        elif num == 3:
            self.bytes_number = 2
            self.description = 'Fuel system status'
            self.unit = ''
        elif num == 4:
            self.bytes_number = 1
            self.description = 'Calculated load engine'
            self.unit = '%'
        if num == 5:
            self.bytes_number = 1
            self.description = 'Engine coolant temperature'
            self.unit = '°C'
        if num == 6:
            self.bytes_number = 1
            self.description = 'Short term fuel trim - Bank 1'
            self.unit = '%'
        if num == 7:
            self.bytes_number = 1
            self.description = 'Long term fuel trim - Bank 1'
            self.unit = '%'
        if num == 8:
            self.bytes_number = 1
            self.description = 'Short term fuel trim - Bank 2'
            self.unit = '%'
        if num == 9:
            self.bytes_number = 1
            self.description = 'Long term fuel trim - Bank 2'
            self.unit = '%'
        if num == 10:
            self.bytes_number = 1
            self.description = 'Fuel pressure'
            self.unit = 'kPa'
        if num == 11:
            self.bytes_number = 1
            self.description = 'Intake manifold absolute pressure'
            self.unit = 'rpm'
        if num == 12:
            self.bytes_number = 2
            self.description = 'Engine speed'
            self.unit = 'km/h'
        if num == 13:
            self.bytes_number = 1
            self.description = 'Vehicle speed'
            self.unit = '%'
        if num == 14:
            self.bytes_number = 1
            self.description = 'Timing advance'
            self.unit = '° before TDC'
        if num == 15:
            self.bytes_number = 1
            self.description = 'Intake air temperature'
            self.unit = '°C'
        if num == 16:
            self.bytes_number = 2
            self.description = 'Air flow rate (MAF)'
            self.unit = 'g/s'
        if num == 17:
            self.bytes_number = 1
            self.description = 'Throttle position'
            self.unit = '%'
        if num == 18:
            self.bytes_number = 1
            self.description = 'Commanded secondary air status'
            self.unit = ''
        if num == 19:
            self.bytes_number = 1
            self.description = 'Oxygen sensor present'
            self.unit = ''
        if num == 20:
            self.bytes_number = 2
            self.description = 'Oxygen sensor 1'
            self.unit = ''
        if num == 21:
            self.bytes_number = 2
            self.description = 'Oxygen sensor 2'
            self.unit = ''
        if num == 22:
            self.bytes_number = 2
            self.description = 'Oxygen sensor 3'
            self.unit = ''
        if num == 23:
            self.bytes_number = 2
            self.description = 'Oxygen sensor 4'
            self.unit = ''
        if num == 24:
            self.bytes_number = 2
            self.description = 'Oxygen sensor 5'
            self.unit = ''
        if num == 25:
            self.bytes_number = 2
            self.description = 'Oxygen sensor 6'
            self.unit = ''
        if num == 26:
            self.bytes_number = 2
            self.description = 'Oxygen sensor 7'
            self.unit = ''
        if num == 27:
            self.bytes_number = 2
            self.description = 'Oxygen sensor 8'
            self.unit = ''
    
    def compute(self, A, B=0, C=0, D=0):
        if self.number == 0:
            pid_support =''
            raw_data = 0x1000000 * A + 0x10000 * B + 0x100 * C + D
            for i in range(32):
                if 0x80000000 >> i & raw_data != 0:
                    pid_support = pid_support + str(i+1) + ' - '
            return pid_support
        elif self.number == 1:
            return 0
        elif self.number == 2:
            return 0
        elif self.number == 3:
            return 0
        elif self.number == 4:
            return 100*A/255
        elif self.number == 5:
            return A - 40
        elif self.number >= 6 and self.number <=9:
            return 100*A / 128 - 100
        elif self.number == 10:
            return 3*A
        elif self.number == 11:
            return A
        elif self.number == 12:
            return (256*A+B)/4
        elif self.number == 13:
            return A
        elif self.number == 14:
            return A/2 - 64
        elif self.number == 15:
            return A-40
        elif self.number == 16:
            return (256*A+B)/100
        elif self.number == 17:
            return 100*A/255
        elif self.number == 18:
            return 0
