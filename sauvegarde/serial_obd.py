#!/usr/bin/python
# -*- coding: utf-8 -*-


import serial

#Initialisation de la communication serie
def serial_init(port, baud=9600):
    print("init")
    serial_com = serial.Serial(port, timeout=None, baudrate = baud)
    return serial_com
	
#Write data on serial port
def serial_write(serial_com, data):
    serial_com.write(bytearray(data, 'ascii'))

#Read data on serial port
def serial_read(serial_com):
    #read until string \r\r
    serialString = serial_com.read_until(b'\r\r')
    return serialString
