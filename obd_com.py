
import serial_obd
import pid

def obd_init(port, data='ATSP0\r'):
    serial_com = serial_obd.serial_init(port)
    serial_obd.serial_write(serial_com, data)
    print("write")
    if serial_obd.serial_read(serial_com).decode("Ascii").find('OK'):
        print('Connexion etablie')
        return serial_com
    else:
        print('Echec de la connexion')
        return serial_com


def pid_write(serial_com, pid):
    serial_obd.serial_write(serial_com, '01') #service 01
    
    #write PID
    if pid.number <= 0x0F: #if PID number <= 15, 0 must be added before
        serial_obd.serial_write(serial_com, '0')
        serial_obd.serial_write(serial_com, hex(pid.number)[2:])
    else:
        serial_obd.serial_write(serial_com, hex(pid.number)[2:])
        
    serial_obd.serial_write(serial_com, '\r')
	
def pid_read(serial_com, bytes_num):
    #read data from OBD
    chaine = serial_obd.serial_read(serial_com)
    
    #suppress spaces if there are some in OBD answer
    chaine = str(chaine).replace(" ", "")
    
     #41 means an answer to service 01
    debut = chaine.find('41')
    
    #if there is no data in anser, find funtion returns -1
    if debut != -1:
        #suppress what is before 41
        chaine = chaine[debut:]
        
        #select bytes of data in chain
        if bytes_num == 1:
            chaine = chaine[4:6]
        elif bytes_num == 2:
            chaine = chaine[4:8]
        elif bytes_num == 3:
            chaine = chaine[4:10]
        elif bytes_num == 4:
            chaine = chaine[4:12]
    else:
        chaine = "NO DATA"
        
    return chaine

def faults_request(serial_com):
    serial_obd.serial_write(serial_com, '03\r')

# request 03; answer 0343 01 13 40
def faults_read(serial_com):
    print("read")
    chaine = serial_obd.serial_read(serial_com)
    print(chaine)
    debut = chaine.decode("Ascii").find('43')
    chaine = chaine[debut:]
    chaine = chaine[3:14]
    print(chaine)
    return chaine
